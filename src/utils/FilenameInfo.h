/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <string>

namespace medialibrary
{

class FilenameInfo
{
public:
    enum class GuessedType
    {                       // Valid fields:
        Unknown,            // No fields are valid
        TVShowEpisode,      // Title, Season, Episode, [Year]
        TVShowPartEpisode,  // TODO
        TVShowMultiEpisode, // TODO
        FullMovie,          // Title, [year]
        PartMovie,          // Title, [year], part number
        ShortMovie,         // Title, [year]
        Trailer,            // Title, [year]
        Sample,             //
        Anime,              // ???
    };

    explicit FilenameInfo( std::string filename );
    ~FilenameInfo() = default;

    bool hasNoInfo() const;

    GuessedType getType() const;

    const std::string& getTitle() const;

    uint16_t getReleaseDate() const;

    uint16_t getSeasonNumber() const;

    uint16_t getEpisodeNumber() const;

// Private methods
private:
    void parse();
    void clearTitle();
    bool extractDate( const std::string& rawTitle );

// Private fields
private:
    // Raw filename
    std::string m_filename;

    // Content guess
    GuessedType m_type;

    // Common fields
    std::string m_clearTitle; // Removed eventual
    uint16_t m_releaseDate;   // Date of first diffusion (of the show itself, not the episode)

    // TVShow specific fields
    uint16_t m_seasonNumber;
    uint16_t m_episodeNumber;

};

}
