/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <regex>
#include <logging/Logger.h>

#include "FilenameInfo.h"

// Regex fragments
#define REG_SNAME     "\\s*(.+?)"
#define REG_DELIM     "[ \\._\\-]"
#define REG_ANYTHING  ".*"
#define REG_RDATE     "(?:19|20)\\d{2}"
#define REG_PRFIX     "(?:[\\{\\[].+[\\}\\]][ \\-]*)"

namespace medialibrary
{

namespace
{

struct Pattern
{
    const char* rationalExp;
    size_t      posTitle;
    size_t      posSeason;
    size_t      posEpisode;
};

const char TO_CLEAR[] = { '.', '_' }; // Symbols to replace with a space in titles

const char* TITLE_DATE_PATTERNS[] = {
        REG_SNAME"(?:" REG_DELIM"(" REG_RDATE"))",
        REG_SNAME"(?:" REG_DELIM"?\\((" REG_RDATE")\\))"
};

Pattern TVSHOWS_PATTERNS[] = {
        // Titles first
        {       // <title>.S<seasonNum>[x]E<episodeNum>
                REG_PRFIX"?"
                REG_SNAME
                REG_DELIM"+"
                "(?:S|seas|season)"
                REG_DELIM"?(20\\d{2}|\\d{1,3})"  // Season Number
                "(?:" REG_DELIM"|x)?"
                "(?:E|ep|episode)"
                REG_DELIM"?(\\d{1,3})"           // Episode Number
                "(?:[^\\d]" REG_ANYTHING")?"
                , 1, 2, 3
        },
        {       // <title>.<seasonNum>x<episodeNum>
                REG_PRFIX"?"
                REG_SNAME
                REG_DELIM"+"
                "\\[?" REG_DELIM"?"
                "(20\\d{2}|\\d{1,3})"            // Season Number
                REG_DELIM"?"
                "x"
                REG_DELIM"?(\\d{1,2})"           // Episode Number
                REG_DELIM"?\\]?"
                "([^\\d]" REG_ANYTHING")?"
                , 1, 2, 3
        },
        {       // <title>.<seasonNum><episodeNum> <- everything is fine...
                REG_PRFIX"?"
                REG_SNAME
                REG_DELIM"+"
                "\\[?" REG_DELIM"?"
                "(\\d{1})"            // Season Number
                "(\\d{2})"            // Episode Number
                REG_DELIM"?\\]?"
                "([^\\d]" REG_ANYTHING")?"
                , 1, 2, 3
        }
        // Seasons/Episode first
};

}

FilenameInfo::FilenameInfo( std::string filename )
    : m_filename{ std::move( filename ) }
    , m_type{ GuessedType::Unknown }
    , m_releaseDate{ 0 }
    , m_seasonNumber{ 0 }
    , m_episodeNumber{ 0 }
{
    parse();
}

void FilenameInfo::parse()
{
    for ( const auto& pattern : TVSHOWS_PATTERNS )
    {
        std::regex showPattern( pattern.rationalExp, std::regex_constants::icase);
        std::smatch matches;

        if ( std::regex_match( m_filename, matches, showPattern ) )
        {
            m_type = GuessedType::TVShowEpisode;

            if ( ! extractDate( matches[pattern.posTitle] ) )
                m_clearTitle = matches[pattern.posTitle];

            m_seasonNumber  = static_cast<uint16_t>( std::stoi( matches[pattern.posSeason] ) );
            m_episodeNumber = static_cast<uint16_t>( std::stoi( matches[pattern.posEpisode] ) );
            break;
        }
    }

    if ( m_type != GuessedType::Unknown )
        clearTitle();

    if ( m_type != GuessedType::Unknown )
        LOG_ERROR("Detected filename:\t", m_clearTitle, " year:\t", m_releaseDate,
                  " season number:\t", m_seasonNumber, " episode number:\t", m_episodeNumber );
}

bool FilenameInfo::extractDate( const std::string& rawTitle )
{
    for ( const auto& titleDate : TITLE_DATE_PATTERNS )
    {
        std::regex datePattern( titleDate, std::regex_constants::icase );
        std::smatch matches;

        if ( ! std::regex_match( rawTitle, matches, datePattern ) )
            continue;

        /*for ( const auto& match : matches )
            LOG_ERROR( match.str() );*/

        m_clearTitle = matches[1];
        try
        {
            m_releaseDate = static_cast<uint16_t>( std::stoi( matches[2] ) );
        }
        catch ( std::invalid_argument& ex ) // remove exception handling
        {
            m_releaseDate = 0;
        }
        return true;
    }
    return false;
}

void FilenameInfo::clearTitle()
{
    for ( char i : TO_CLEAR )
        std::replace( m_clearTitle.begin(), m_clearTitle.end(), i, ' ' );
    while ( m_clearTitle.back() == ' ' )
        m_clearTitle.pop_back();
}

bool FilenameInfo::hasNoInfo() const
{
    return m_type == FilenameInfo::GuessedType::Unknown;
}


FilenameInfo::GuessedType FilenameInfo::getType() const
{
    return m_type;
}

const std::string& FilenameInfo::getTitle() const
{
    return m_clearTitle;
}

uint16_t FilenameInfo::getReleaseDate() const
{
    return m_releaseDate;
}

uint16_t FilenameInfo::getSeasonNumber() const
{
    return m_seasonNumber;
}

uint16_t FilenameInfo::getEpisodeNumber() const
{
    return m_episodeNumber;
}

}

#undef REG_SNAME
#undef REG_DELIM
#undef REG_ANYTHING
#undef REG_RDATE
#undef REG_PRFIX
