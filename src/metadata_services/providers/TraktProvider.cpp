/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "TraktProvider.h"

#include "access/CurlHttp.h"
#include "logging/Logger.h"

namespace medialibrary
{

namespace provider
{

const unsigned int TraktProvider::API_VERSION = 2u;

TraktProvider::TraktProvider( const std::string& traktClientId )
    : IMetadataProvider()
    , m_hostname{ "https://api.trakt.tv/" }
    , m_traktClientId{ traktClientId }
{
    m_name = "Trakt";
    m_availableSearches = SearchServices::Movie | SearchServices::Show | SearchServices::Person;

    m_availableIdLookup = SearchServices::All;
    m_availableImages = ImagesSupport{}; // None
}

IMetadataProvider::MovieResult TraktProvider::searchMovie( const std::string& title )
{
    return MovieResult{};
}

IMetadataProvider::TVShowResult TraktProvider::searchTVShow( const std::string& title )
{
    // TODO: Add year in prototype
    TVShowResult res{};
    res.found = false;

    access::Http::header_t header = { { "Content-Type",      "application/json" },
                                      { "trakt-api-version", std::to_string( API_VERSION ) },
                                      { "trakt-api-key",     m_traktClientId } };

    auto& http = access::Http::Instance();

    auto req = http.get( m_hostname + "search/show", { { "query",    title },
                                                       { "page",     "1" },
                                                       { "limit",    "1" },
                                                       { "extended", "full" } }, header );
    res.timeout = req.timeout;
    // Error check
    if ( req.statusCode != 200 /*|| req.contentType.substr(0, 16) != "application/json"*/ )
        return res; // TODO: Better error/fail propagation

    res.found = true;
    try
    {
        auto jRes  = json::parse( req.payload );
        //LOG_DEBUG( jRes.dump( 2 ) );
        auto show  = jRes[0]["show"];
        auto ids   = show["ids"];

        res.title     = show["title"]; // Should throw if no title
        res.year      = show["year"];
        res.titleSlug = ids["slug"];

        res.tmdbId    = ids["tmdb"]; // Mandatory ID
        res.imdbId    = ids["imdb"]; // Mandatory ID
        res.tvdbId    = toUint( ids["tvdb"] );
        res.tvrageId  = toUint( ids["tvrage"] ) ; // Frequently null

        res.country       = show["country"];
        res.language      = show["language"];
        res.network       = show["network"];
        res.summary       = show["overview"];
        res.trailer       = toString( show["trailer"] );
        res.certification = show["certification"];

        for ( const auto& genre : show["genres"] )
            res.genres.push_back( genre );
    }
    catch ( json::parse_error& e )
    {
        LOG_ERROR( "JSON parsing: ", e.what() );
    }

    return res;
}

}

}
