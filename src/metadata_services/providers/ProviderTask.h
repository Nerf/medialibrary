/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <memory>
#include <vector>

#include "medialibrary/IMediaLibrary.h"

#include "database/DatabaseHelpers.h"
#include "utils/FilenameInfo.h"

namespace medialibrary
{

namespace provider
{
class ProviderTask;
}

namespace policy
{

struct ProviderTaskTable
{
    static const std::string Name;
    static const std::string primaryKeyColumn;
    static int64_t provider::ProviderTask::*const PrimaryKey;
};

}

namespace provider
{

class ProviderTask : public DatabaseHelpers<ProviderTask, policy::ProviderTaskTable>
{
public:
    ProviderTask( MediaLibraryPtr ml, int64_t mediaId, FilenameInfo info );
    ~ProviderTask() = default;

    int64_t id() const;
    bool alive() const;
    uint64_t attempts() const;
    int64_t mediaId() const;
    std::string title() const;
    FilenameInfo::GuessedType type() const;
    uint64_t releaseYear() const;
    uint64_t seasonNumber() const;
    uint64_t episodeNumber() const;

    void save();
    void erase();
    void increaseAttempts();

private:
    MediaLibraryPtr m_ml;

    int64_t     m_id;
    bool        m_alive;      // If false, the task will be deleted by the manager
    uint64_t     m_attempts;

    // Field populated by a ParserService
    int64_t     m_mediaId;

    // Fields populated by the FilenameInfo helper
    std::string m_title;
    uint8_t     m_guessedType; // Cast from FileNameInfo::GuessedType
    uint64_t    m_releaseYear;
    uint64_t    m_episodeNumber;
    uint64_t    m_seasonNumber;

    // Meta
    uint64_t m_dateAdded;
};

}

}
