/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <atomic>

#include "utils/json.hpp"

namespace medialibrary
{

namespace provider
{

using json = nlohmann::json;

class IMetadataProvider
{
public:
    enum class SearchServices : uint8_t
    {
        None    = 0,

        Movie   = 1,
        Show    = 1 << 1,
        Person  = 1 << 2,
        Season  = 1 << 3,
        Episode = 1 << 4,

        All = Movie | Show | Person | Season | Episode
    };

    // For each available types above, tell what images are supported
    enum class ImageServices : uint8_t
    {
        None        = 0,

        Poster      = 1,
        Fanart      = 1 << 1,
        Banner      = 1 << 2,
        Logo        = 1 << 3,
        ClearArt    = 1 << 4,
        Thumb       = 1 << 5,
        Screenshot  = 1 << 6,
        Character   = 1 << 7
    };

    enum class TVShowStatus
    {
        Running,
        Ended,
        // ? Returning?
    };

    // RANDOM FIXME: GLOBAL LOCK for full show fetching, or use map/set of shows?
    // FIXME: Store images url (lazy image fetching)? Used title table to match show
    // Find a way to ease manual correction/search/reclassification
    // Person <-> Role/job (maybe separate 'em) <- (role/episode relation | role/movie relation) -> Show/Movie
    // Client-side rate limit (uint64_t nbRequests // uint64_t lastFlushTimestamp)
    // Finish ProviderTask & create new tables

    struct ImagesSupport
    {
        ImagesSupport()
            : movie{ ImageServices::None }
            , show{ ImageServices::None }
            , person{ ImageServices::None }
            , season{ ImageServices::None }
            , episode{ ImageServices::None }
        {}

        ImageServices movie;
        ImageServices show;
        ImageServices person;
        ImageServices season;
        ImageServices episode;
    };

    struct PersonResult
    {
        std::string imdbId;
        uint64_t    tmdbId;
        std::string nextId;

        std::string fullName;

        // Movie/Show crew related info
        std::string departement;
        std::string job;

        // Movie/Show actors related info
        std::string character;
        std::string imagePath;

        // Person info
        std::string birthday;
        std::string deathday;
        std::string shortbio;
        std::string placeOfBirth;

        bool        gender;
    };

    struct TVShowEpisodeResult
    {
        std::string imdbId;
        uint64_t    tmdbId;
        std::string nextId;

        std::string airDate;
        std::vector<PersonResult> crew;
        std::vector<PersonResult> guestStars;
        uint64_t episodeNumber;
        uint64_t seasonNumber; // Useless (already in tree)?
        std::string name;
        std::string summary; // overview
        std::string screenshotPath;
    };

    struct TVShowSeasonResult
    {
        std::string imdbId;
        uint64_t    tmdbId;
        std::string nextId;

        std::string airDate;
        std::vector<TVShowEpisodeResult> episodes;
        std::string name;
        std::string overview;
        std::string posterPath;
        uint64_t    seasonNumber;
    };

    struct TVShowResult
    {
        bool found;
        bool timeout;
        std::string title;
        std::string titleSlug;
        uint64_t year;

        std::string imdbId;
        uint64_t tmdbId;
        std::string nextId;
        uint64_t tvdbId;
        uint64_t tvmazeId;
        uint64_t tvrageId;

        std::string country;
        std::string language; // original language
        std::string network;
        std::string summary;
        std::string trailer;
        std::string certification;
        std::vector<std::string> genres; // TODO struct holding genre-id

        std::string tmdbPosterPath;
        std::string tmdbBackdropPath;

        std::vector<PersonResult> creators;
        std::vector<TVShowSeasonResult> seasons;

        TVShowStatus status; // Or use "in_production"?
        std::string type; // TODO Struct (w/ TMDb id)? Enum?
    };

    struct MovieResult
    {
        bool found;
        bool timeout;
        std::string title;
        std::string titleSlug;
        uint64_t year;

        std::string imdbId;
        uint64_t    tmdbId;
        std::string nextId;

        std::string language;
        std::string summary;
        std::vector<std::string> genres;

        std::string tmdbPosterPath;
        std::string tmdbBackdropPath;

        // TODO To complete with info from id search
    };

    IMetadataProvider()
    : m_availableSearches{ SearchServices::None }
    , m_availableIdLookup{ SearchServices::None }
    {}

    virtual ~IMetadataProvider() = default;
    //explicit IMetadataProvider( const std::string& hostname );
    //IMetadataProvider( const std::string& hostname, const std::string& apiKey );

    // TODO Setters for image resolution, etc...

    virtual MovieResult searchMovie( const std::string& title ) = 0;

    virtual TVShowResult searchTVShow( const std::string& title ) = 0;

    //virtual bool episodeInfos( const std::string& title ) = 0;

    virtual std::string name()
    {
        return m_name;
    }

    virtual SearchServices availableSearches()
    {
        return m_availableSearches;
    }

    virtual SearchServices availableLookup()
    {
        return m_availableIdLookup;
    }

    virtual ImagesSupport avaiableImages()
    {
        return m_availableImages;
    }

protected:
    std::string toString( const json& jsonNode )
    {
        if ( jsonNode.is_null() )
            return {};
        return jsonNode;
    }

    uint64_t toUint( const json& jsonNode )
    {
        if ( jsonNode.is_null() )
            return 0;
        return jsonNode;
    }

    std::string    m_name;
    SearchServices m_availableSearches;
    SearchServices m_availableIdLookup;

    ImagesSupport  m_availableImages;

};

template <typename T, typename = std::enable_if<std::is_enum<T>::value>>
T operator|( const T& s1, const T& s2 )
{
    return static_cast<T>( static_cast<uint8_t>( s1 ) | static_cast<uint8_t>( s2 ) );
}

template <typename T, typename = std::enable_if<std::is_enum<T>::value>>
T& operator|=( T& s1, const T& s2 )
{
    s1 = s1 | s2;
    return s1;
}

template <typename T, typename = std::enable_if<std::is_enum<T>::value>>
T operator&( const T& s1, const T& s2 )
{
    return static_cast<T>( static_cast<uint8_t>( s1 ) & static_cast<uint8_t>( s2 ) );
}

template <typename T, typename = std::enable_if<std::is_enum<T>::value>>
T& operator&=( T& s1, const T& s2 )
{
    s1 = s1 & s2;
    return s1;
}

template <typename T, typename = std::enable_if<std::is_enum<T>::value>>
bool operator%( const T& s1, const T& s2 )
{
    return static_cast<bool>( s1 & s2 );
}

}

}
