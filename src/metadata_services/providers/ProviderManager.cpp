/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "ProviderManager.h"

#include "ProviderTask.h"
#include "IMetadataProvider.h"

namespace medialibrary
{

namespace provider
{

ProviderManager::ProviderManager( MediaLibrary* ml )
    : m_ml{ ml }
    , m_paused{ false }
    , m_stopped{ true }
{}

ProviderManager::~ProviderManager()
{
    stop();
}

void ProviderManager::addProvider( ProviderPtr provider )
{
    std::unique_lock<compat::Mutex> l( m_providersLock );
    m_providers.emplace( std::piecewise_construct,
                         std::forward_as_tuple( provider ),
                         std::forward_as_tuple( 0 ) );

    // fill-up priority queues
    if ( provider->availableSearches() % IMetadataProvider::SearchServices::Movie )
        m_movieSearchProviders.list.emplace_back( provider );
    if ( provider->availableSearches() % IMetadataProvider::SearchServices::Show )
        m_showSearchProviders.list.emplace_back( provider );
    if ( provider->availableSearches() % IMetadataProvider::SearchServices::Person )
        m_peopleSearchProviders.list.emplace_back( provider );
}

void ProviderManager::addTask( ProviderTaskPtr task )
{
    std::unique_lock<compat::Mutex> l( m_tasksLock );
    m_tasks.emplace( std::move( task ) );
    m_cond.notify_one();
}

void ProviderManager::start()
{
    assert( m_stopped );
    assert( m_threads.empty() );

    // Create threads
    auto nbThreads = compat::Thread::hardware_concurrency();
    if ( nbThreads == 0 )
        nbThreads = 1;

    m_stopped = false;
    for ( auto i = 0u; i < nbThreads; ++i )
        m_threads.emplace_back( &ProviderManager::processTasks, this );
}

void ProviderManager::pause()
{
    m_paused = true;
}

void ProviderManager::resume()
{
    m_paused = false;
    m_cond.notify_all();
}

void ProviderManager::stop()
{
    m_stopped = true;
    m_paused  = false;
    m_cond.notify_all();

    for ( auto& t : m_threads )
        t.join();
}

bool ProviderManager::reloadTasks()
{
    assert( m_stopped );
    if ( ! m_tasks.empty() )
        return false;

    // Delete tasks with retry >= 3 ? Maybe no internet
    // Fetch tasks from database

    return true;
}

void ProviderManager::processTasks()
{
    TaskQueue localTasks; // Other workers are dealing with information related to these tasks
    for (;;)
    {
        ProviderTaskPtr currentTask;
        if ( localTasks.empty() )
        {
            std::unique_lock<compat::Mutex> l( m_tasksLock );
                m_cond.wait( l, [this]() {
                    return m_stopped || !m_paused || !m_tasks.empty();
                } );

            if ( m_stopped )
                break;

            currentTask = std::move( m_tasks.front() );
            m_tasks.pop();
        }
        else
        {
            // Check if task available, maybe need waitcond / lock on it,
            // maybe the same lock as the shows set, maybe on another routine
            {
                std::unique_lock<compat::Mutex> l( m_tasksLock );
                // Wait for something? like a
            }

            currentTask = std::move( localTasks.front() );
            localTasks.pop();
        }
        runTask( std::move( currentTask ) );
    }
}

void ProviderManager::runTask( ProviderTaskPtr&& task )
{
    if ( task->alive() == false )
    {
        // Delete entry, the mirror-object is destructed here too
        task->erase();
        return;
    }
    auto success = Fate::End;
    switch ( task->type() )
    {
        case FilenameInfo::GuessedType::FullMovie:

            break;

        case FilenameInfo::GuessedType::TVShowEpisode:
            success = pullTVShowEpisodeInfos( task );
            break;

        default:
            assert( false );
            break;
    }
    switch ( success )
    {
        case Fate::Retry:
            if ( task->attempts() >= 3 )
            {
                task->erase();
                break;
            }
            task->increaseAttempts(); // It synchronize to DB as well
            {
                std::unique_lock<compat::Mutex> l( m_tasksLock );
                m_tasks.emplace( std::move( task ) );
            }
            break;
        case Fate::End:
            task->erase();
            break;
        case Fate::Postpone:
            // The task isn't erased from DB but cleared in queue,
            // will be fetched at next reload, hopefully
            break;
    }
}

ProviderManager::ProviderPtr ProviderManager::next( ProviderCircular& queue )
{
    // Fairly use each capable providers to balance the charge and deal with rate limits
    std::unique_lock<compat::Mutex> l( m_providersLock );
    if ( queue.list.empty() )
        return nullptr;
    assert( queue.position < queue.list.size() );
    auto res = queue.list[queue.position];
    queue.position = ++queue.position % queue.list.size();
    return res;
}

void ProviderManager::disqualify( ProviderPtr& provider )
{
    std::unique_lock<compat::Mutex> l( m_providersLock );
    disqualify( provider, m_showSearchProviders );
    disqualify( provider, m_movieSearchProviders );
    disqualify( provider, m_peopleSearchProviders );
}

void ProviderManager::disqualify( ProviderPtr& provider, ProviderCircular& pool )
{
    if ( pool.list.empty() )
        return;
    auto pos = std::find( pool.list.begin(), pool.list.end(), provider );
    if ( pos == pool.list.end() )
        return;
    pool.list.erase( pos );
    pool.position = 0;
}

ProviderManager::Fate ProviderManager::pullTVShowEpisodeInfos( const ProviderTaskPtr& task )
{
    if ( task->title().empty() )
        return Fate::End; // Delete this useless task

    auto provider = next( m_showSearchProviders );
    if ( provider == nullptr )
        return Fate::Postpone; // No provider currently available for this task, save it for later

    if ( m_providers[provider] < 0 )
    {
        disqualify( provider );
        return Fate::Retry;
    }

    // If show not known in DB, use show-specific mutex?
    auto fetchedData = provider->searchTVShow( task->title() );
    if ( fetchedData.timeout && ! m_stopped )
    {
        m_providers[provider] -= 10; // Degrade provider
        return Fate::Retry;
    }
    if ( ! fetchedData.found )
    {
        return Fate::Retry; // But blacklist provider for this task? or Fate::End, avoiding waisting requests?
    }
    ++m_providers[provider];

    return Fate::End;
}

}

}