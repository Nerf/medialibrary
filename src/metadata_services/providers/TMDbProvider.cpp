/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "TMDbProvider.h"

#include "access/CurlHttp.h"
#include "logging/Logger.h"

namespace medialibrary
{

namespace provider
{

const unsigned int TMDbProvider::API_VERSION = 3u;

TMDbProvider::TMDbProvider( const std::string& TMDbApiKey )
        : IMetadataProvider()
        , m_TMDbApiKey{ TMDbApiKey }
{
    m_name = "TMDb";
    m_hostname = "https://api.themoviedb.org/" + std::to_string( API_VERSION ) + '/';

    m_availableSearches = SearchServices::Movie | SearchServices::Show | SearchServices::Person;
    m_availableIdLookup = SearchServices::All;

    m_availableImages = ImagesSupport{};
    m_availableImages.show    = ImageServices::Poster | ImageServices::Fanart;
    m_availableImages.season  = ImageServices::Poster;
    m_availableImages.episode = ImageServices::Screenshot;
    m_availableImages.movie   = ImageServices::Poster | ImageServices::Fanart;
    m_availableImages.person  = ImageServices::Character;
}

IMetadataProvider::MovieResult TMDbProvider::searchMovie( const std::string& title )
{
    MovieResult res{};
    res.found = false;

    auto& http = access::Http::Instance();

    http.setUserAgent("test/21.2"); // FIXME: Remove

    auto req = http.get( m_hostname + "search/movie", {  { "api_key",  m_TMDbApiKey },
                                                         { "language", "en-US"},
                                                         { "query",    title },
                                                         { "page",     "1" } }, {} );
    res.timeout = req.timeout;
    if ( req.statusCode != 200 )
        return res;

    res.found = true;
    try
    {
        auto jRes = json::parse( req.payload );
        auto movie = jRes["results"][0];
        res.tmdbId = movie["id"];
        res.title = movie["original_title"];
        std::string date = movie["release_date"];
        res.year = static_cast<uint64_t>( std::stoi( date.substr(0, 4) ) );
        res.language = movie["original_language"];
        res.summary = movie["overview"];
        res.tmdbPosterPath = movie["poster_path"];
        res.tmdbBackdropPath = movie["backdrop_path"];
    }
    catch ( json::parse_error& e )
    {
        LOG_ERROR( "JSON parsing: ", e.what() );
    }

    return res;
}

IMetadataProvider::TVShowResult TMDbProvider::searchTVShow( const std::string& title )
{
    TVShowResult res{};
    res.found = false;

    auto& http = access::Http::Instance();

    http.setUserAgent("test/21.2"); // FIXME: Remove

    auto req = http.get( m_hostname + "search/tv", { { "api_key",  m_TMDbApiKey },
                                                     { "language", "en-US"},
                                                     { "query",    title },
                                                     { "page",     "1" } }, {} );
    res.timeout = req.timeout;
    if ( req.statusCode != 200 )
        return res;

    res.found = true;
    try
    {
        auto jRes = json::parse( req.payload );
        //LOG_DEBUG( jRes.dump( 2 ) );
        auto show = jRes["results"][0];
        res.tmdbId = show["id"];
        res.title = show["original_name"];
        std::string date = show["first_air_date"];
        res.year = static_cast<uint64_t>( std::stoi( date.substr(0, 4) ) );
        res.language = show["original_language"];
        res.country = show["origin_country"][0];
        res.summary = show["overview"];
        res.tmdbPosterPath = show["poster_path"];
        res.tmdbBackdropPath = show["backdrop_path"];
    }
    catch ( json::parse_error& e )
    {
        LOG_ERROR( "JSON parsing: ", e.what() );
    }

    return res;
}

}

}