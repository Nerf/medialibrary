/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include <queue>
#include <unordered_map>
#include <unordered_set>

#include "MediaLibrary.h"

#include "compat/ConditionVariable.h"
#include "compat/Mutex.h"
#include "compat/Thread.h"
#include "access/IHttp.h"

namespace medialibrary
{

namespace provider
{

class IMetadataProvider;
class ProviderTask;

class ProviderManager
{
public:
    using ProviderPtr     = std::shared_ptr<IMetadataProvider>;
    using ProviderTaskPtr = std::unique_ptr<ProviderTask>;

    explicit ProviderManager( MediaLibrary* ml );
    ~ProviderManager();
    ProviderManager( const ProviderManager& ) = delete;
    ProviderManager& operator=( const ProviderManager& ) = delete;

    void addProvider( ProviderPtr provider );

    void addTask( ProviderTaskPtr task );

    bool reloadTasks(); // May be called before start() to fill-up initial work for threads, or after manager went idle.

    void start();  // create threads if empty, fetch tasks if empty
    void pause();  // must be called on connexion lost || GSM (according to preferences)
    void resume();
    void stop();   // Join threads

private:
    using ProviderList = std::vector<ProviderPtr>;
    using TaskQueue    = std::queue<ProviderTaskPtr>;

private:
    struct ProviderCircular
    {
        ProviderList list{};
        uint64_t position{ 0 };
    };

    enum class Fate
    {
        End,
        Retry,
        Postpone
    };

private:
    void createTVShow( const ProviderTask& task );
    void createSeason( const ProviderTask& task );
    void createMovie( const ProviderTask& task );
    void createPeople( const ProviderTask& task );

    Fate pullTVShowEpisodeInfos( const ProviderTaskPtr& task );

    void processTasks();
    void runTask( ProviderTaskPtr&& task );

    void providerMonitor( const ProviderTask& task, access::IHttp::Response ); // Dynamically manage priority between providers

    ProviderPtr next( ProviderCircular& queue );
    void disqualify( ProviderPtr& provider );
    void disqualify( ProviderPtr& provider, ProviderCircular& pool );

private:
    MediaLibrary*    m_ml;
    IMediaLibraryCb* m_callback;

    std::unordered_map<ProviderPtr, std::atomic_int64_t> m_providers; // All providers w/ their mark

    std::unordered_set<std::string> m_showsInProcess;
    compat::Mutex                   m_showsInProcessLock;
    compat::ConditionVariable       m_showsInProcessCond;

    ProviderCircular m_movieSearchProviders;
    ProviderCircular m_showSearchProviders;
    ProviderCircular m_peopleSearchProviders;

    // Not sure if needed, I think not
    ProviderCircular m_movieLookupProviders;
    ProviderCircular m_showLookupProviders;
    ProviderCircular m_peopleLookupProviders;

    // weeell maybe after all
    ProviderCircular m_imageProviders;

    TaskQueue    m_tasks;

    std::atomic_bool m_paused;
    std::atomic_bool m_stopped;

    std::vector<compat::Thread> m_threads;
    compat::Mutex               m_tasksLock;
    compat::Mutex               m_providersLock;
    compat::ConditionVariable   m_cond;
};

}

}
