/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "ProviderTask.h"

namespace medialibrary
{

namespace provider
{


ProviderTask::ProviderTask( MediaLibraryPtr ml, int64_t mediaId, FilenameInfo info )
    : m_ml{ ml }
    , m_alive{ true }
    , m_attempts{ 0 }
    , m_mediaId{ mediaId }
    , m_title{ info.getTitle() }
    , m_guessedType{ static_cast<uint8_t>( info.getType() ) }
    , m_releaseYear{ info.getReleaseDate() }
    , m_episodeNumber{ info.getEpisodeNumber() }
    , m_seasonNumber{ info.getSeasonNumber() }
{}

int64_t ProviderTask::id() const
{
    return m_id;
}

bool ProviderTask::alive() const
{
    return m_alive;
}

FilenameInfo::GuessedType ProviderTask::type() const
{
    return static_cast<FilenameInfo::GuessedType>( m_guessedType );
}

std::string ProviderTask::title() const
{
    return m_title;
}

uint64_t ProviderTask::releaseYear() const
{
    return m_releaseYear;
}

uint64_t ProviderTask::seasonNumber() const
{
    return m_seasonNumber;
}

uint64_t ProviderTask::episodeNumber() const
{
    return m_episodeNumber;
}

void ProviderTask::save()
{
    // Do not save if retry > 3 ?
}

void ProviderTask::erase()
{

}

void ProviderTask::increaseAttempts()
{
    ++m_attempts;
    // Update database
}

uint64_t ProviderTask::attempts() const
{
    return m_attempts;
}

int64_t ProviderTask::mediaId() const
{
    return m_mediaId;
}

}

}
