/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <cassert>
#include <compat/Mutex.h>
#include <fstream>
#include <logging/Logger.h>
#include <utils/Url.h>
#include <utils/Filename.h>

#include "CurlHttp.h"

namespace medialibrary
{

namespace access
{

const char* const DEFAULT_USER_AGENT = "medialibrary/" VERSION;

static size_t stringWrite( char* ptr, size_t size, size_t nmemb, std::string* data )
{
    assert( ptr  != nullptr );
    assert( data != nullptr );

    auto bytesWritten = size * nmemb;
    data->append( ptr, bytesWritten );
    return bytesWritten;
}

static size_t fileWrite( char* ptr, size_t size, size_t nmemb, std::ofstream* data )
{
    auto bytesWritten = size * nmemb;
    data->write( ptr, bytesWritten );
    return bytesWritten;
}

static void lock(CURL*, curl_lock_data data, curl_lock_access, void* userdata)
{
    // Use unique_lock?
    auto mutices = static_cast<compat::Mutex*>(userdata);
    mutices[data - 1].lock();
}

static void unlock(CURL*, curl_lock_data data, void* userdata)
{
    auto mutices = static_cast<compat::Mutex*>(userdata);
    mutices[data - 1].unlock();
}

Http::Http()
    : m_userAgent( DEFAULT_USER_AGENT )
{
    curl_global_init( CURL_GLOBAL_DEFAULT );

    m_curlShare = curl_share_init();

    // Share unused connexion pool, cookies, DNS cache and TLS session IDs between concurrent easy handlers
    for ( auto i : LOCK_SHARES )
        curl_share_setopt( m_curlShare, CURLSHOPT_SHARE, i );

    curl_share_setopt( m_curlShare, CURLSHOPT_LOCKFUNC,   lock );
    curl_share_setopt( m_curlShare, CURLSHOPT_UNLOCKFUNC, unlock );
    curl_share_setopt( m_curlShare, CURLSHOPT_USERDATA,   m_mutices );

    LOG_INFO( "cURL singleton instantiated" );
}

Http::~Http() // Only called at application teardown
{
    curl_share_cleanup( m_curlShare );
    curl_global_cleanup();

    LOG_INFO( "cURL singleton destructed" );
}

std::string Http::serializeGetParameters( const param_t& parameters )
{
    std::string res;
    bool firstPair = true;
    for ( const auto& p : parameters )
    {
        if ( p.first.empty() == true || p.second.empty() == true )
            continue;
        res += firstPair ? '?' : '&';
        firstPair = false;
        res += utils::url::encode( p.first ) + '=' + utils::url::encode( p.second );
    }
    return res;
}

struct curl_slist* Http::serializeHeaders( const header_t& headers )
{
    curl_slist* res = nullptr;
    for ( const auto& h : headers )
    {
        auto prepHeader = h.first + ':' + h.second;
        res = curl_slist_append( res, prepHeader.c_str() );
    }
    return res;
}

void Http::curlSetOptions( CURL* handler, const std::string& preparedUrl, curl_slist* preparedHeaders )
{
    curl_easy_setopt( handler, CURLOPT_URL,             preparedUrl.c_str() );
    curl_easy_setopt( handler, CURLOPT_USERAGENT,       m_userAgent.c_str() );
    curl_easy_setopt( handler, CURLOPT_HTTPHEADER,      preparedHeaders );
    curl_easy_setopt( handler, CURLOPT_FOLLOWLOCATION,  1L );
    curl_easy_setopt( handler, CURLOPT_MAXREDIRS,       10L );
    curl_easy_setopt( handler, CURLOPT_SHARE,           m_curlShare ); // Connect easy handler to the shared one
    curl_easy_setopt( handler, CURLOPT_TIMEOUT,         5L);
}

void Http::getInfos( CURL* handler, unsigned int& statusCode, double& elapsed, std::string& contentType )
{
    curl_easy_getinfo( handler, CURLINFO_RESPONSE_CODE, &statusCode );
    curl_easy_getinfo( handler, CURLINFO_TOTAL_TIME,    &elapsed );

    char* ct = nullptr;
    auto ctExtraction = curl_easy_getinfo( handler, CURLINFO_CONTENT_TYPE, &ct );
    if ( ctExtraction && ct )
        contentType = ct;
}

bool Http::performRequest( CURL* handler, Response& response )
{
    CURLcode returnCode;
    try
    {
        // synchronous call, block current thread
        returnCode = curl_easy_perform( handler );
    }
    catch ( std::system_error& ex )
    {
        // Exception thrown, so current thread cannot be blocked by a race condition caused by failure
        LOG_ERROR( "Impossible to lock or unlock a cURL ressource, shared ressources are in indefinite state" );
        // Attempt to recover shared resources state by cleaning up isn't an option,
        // since they may be being used at the same time
        return false;
    }
    response.timeout = returnCode == CURLE_OPERATION_TIMEDOUT;
    return returnCode == CURLE_OK;
}

Http::Response Http::get( const std::string& url, const param_t& parameters, const header_t& headers )
{
    Http::Response res{};
    CurlEasyHandler curl;
    if ( curl() == nullptr )
        return res;

    if ( utils::file::schemeIs( "http://", url ) == false && utils::file::schemeIs( "https://", url ) == false )
        return res;

    auto preparedUrl = url + serializeGetParameters( parameters );
    curl_slist* preparedHeaders = nullptr;
    if ( headers.empty() == false )
        preparedHeaders = serializeHeaders( headers );

    curlSetOptions( curl(), preparedUrl, preparedHeaders );

    curl_easy_setopt( curl(), CURLOPT_WRITEFUNCTION,   stringWrite );
    curl_easy_setopt( curl(), CURLOPT_WRITEDATA,       &res.payload );
    curl_easy_setopt( curl(), CURLOPT_HEADERDATA,      &res.header );

    LOG_INFO( "Starting HTTP GET request on endpoint ", preparedUrl );

    if ( ! performRequest( curl(), res ) )
    {
        curl_slist_free_all( preparedHeaders );
        LOG_ERROR( "Request of ", preparedUrl, " failed" );
        return res;
    }

    double elapsed;
    getInfos( curl(), res.statusCode, elapsed, res.contentType );

    LOG_INFO( "Request ", preparedUrl, " ended in ", elapsed,"s, with return code ", res.statusCode );

    curl_slist_free_all( preparedHeaders );
    return res;
}

Http::Response Http::getFile( const std::string& url, const std::string& destinationName )
{
    // Hotlink a file an write it on file-system
    Response res{};

    CurlEasyHandler curl;
    if ( curl() == nullptr )
        return res;

    if ( utils::file::schemeIs( "http://", url ) == false && utils::file::schemeIs( "https://", url ) == false )
        return res;

    curlSetOptions( curl(), url, nullptr );

    std::ofstream outputFile{ destinationName, std::ofstream::binary };

    curl_easy_setopt( curl(), CURLOPT_WRITEFUNCTION, fileWrite );
    curl_easy_setopt( curl(), CURLOPT_WRITEDATA,     &outputFile );

    LOG_INFO( "Starting HTTP GET request on file ", url, " -> ", destinationName );
    if ( ! performRequest( curl(), res ) )
        return res;

    double elapsed;
    getInfos( curl(), res.statusCode, elapsed, res.contentType );

    LOG_INFO( "Request of file ", url, " ended in ", elapsed,"s, with return code ", res.statusCode );

    return res;
}

}

}
