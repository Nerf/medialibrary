/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <string>
#include <vector>

namespace medialibrary
{

namespace access
{

// Lazy initialized thread-safe singleton
template <typename T>
class Singleton
{
public:
    static T& Instance()
    {
        static T res;
        return res;
    }
};

class IHttp
{
public:

    using auth_t   = std::pair<std::string, std::string>;              // {username, password}
    using param_t  = std::vector<std::pair<std::string, std::string>>; // serialized to ?key=value&...
    using header_t = std::vector<std::pair<std::string, std::string>>;

    struct Response
    {
        unsigned int statusCode;
        bool         timeout;
        std::string  header;
        std::string  contentType;
        std::string  payload;
    };

    IHttp( const IHttp& ) = delete;
    virtual IHttp& operator=( const IHttp& ) = delete;

    virtual void setUserAgent( const std::string& userAgent ) = 0;

    virtual Response getFile( const std::string& url, const std::string& filename ) = 0;

    virtual Response get( const std::string& url, const param_t& parameters, const header_t& headers ) = 0;

protected: // Making the object non instantiable without calling `Instance()`
    IHttp() = default;
    virtual ~IHttp() = default;
};

}

}
