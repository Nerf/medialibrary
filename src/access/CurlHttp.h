/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2017 Hugo Beauzée-Luyssen, Videolabs
 *
 * Authors: Hugo Beauzée-Luyssen<hugo@beauzee.fr>
 *          Alexandre Fernandez <nerf@boboop.fr>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once
#include <mutex>
#include <curl/curl.h>

#include "IHttp.h"

namespace medialibrary
{

namespace compat
{

using Mutex = std::mutex; // FIXME

}

namespace access
{

class Http : public IHttp, public Singleton<Http>
{
public:

    virtual void setUserAgent( const std::string& userAgent ) override
    {
        m_userAgent = userAgent;
    }

    virtual Response getFile( const std::string& url, const std::string& destinationName ) override;

    virtual Response get( const std::string& url, const param_t& parameters, const header_t& headers ) override;

private:
    Http();
    ~Http();
    friend class Singleton<Http>;

private:

    class CurlEasyHandler
    {
    public:
        CurlEasyHandler()
        {
            handler = curl_easy_init();
        }
        ~CurlEasyHandler()
        {
            curl_easy_cleanup( handler );
        }

        CURL* operator()()
        {
            return handler;
        }

    private:
        CURL* handler;
    };

    static std::string serializeGetParameters( const param_t& parameters );
    static struct curl_slist* serializeHeaders( const header_t& headers );
    void curlSetOptions( CURL* handler, const std::string& preparedUrl, curl_slist* preparedHeaders );
    static bool performRequest( CURL* handler, Response& response );
    static void getInfos( CURL* handler, unsigned int& statusCode, double& elapsed, std::string& contentType );

    const curl_lock_data LOCK_SHARES[5] = {
            CURL_LOCK_DATA_SHARE,
            CURL_LOCK_DATA_COOKIE,
            CURL_LOCK_DATA_DNS,
            CURL_LOCK_DATA_SSL_SESSION,
            CURL_LOCK_DATA_CONNECT
    };

    CURLSH*       m_curlShare;
    std::string   m_userAgent;
    compat::Mutex m_mutices[sizeof ( LOCK_SHARES ) / sizeof ( curl_lock_data )];
};

}

}
